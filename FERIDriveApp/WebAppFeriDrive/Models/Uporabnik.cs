using System;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

using System.Collections.Generic;

using WebAppFeriDrive.Models;

public class Uporabnik
{
    private readonly int id;
    public string uporabniskoIme;
    private string geslo;
    public string email;
    private string telefonska;
    private string telPotrjena;
    public string ime;
    public string priimek;
    private double avgOcena;
    protected Prevoz seznamPrevozov;
    private List<Ocena> ocene;
    protected Prevoz[] prevozs;
    protected Pogovor[] pogovors;
    protected RezultatIskanja rezultatIskanja;

    public string Geslo { get => geslo; set => geslo = value; }
    public string Telefonska { get => telefonska; set => telefonska = value; }
    public string TelPotrjena { get => telPotrjena; set => telPotrjena = value; }
    public int Id => id;
    public List<Ocena> Ocene { get => ocene; set => ocene = value; }
    public double AvgOcena { get => avgOcena; set => avgOcena = value; }

    public Uporabnik(int id, string uporabniskoIme, string geslo, string email, string telefonska, string telPotrjena, string ime, string priimek)
    {
        this.id = id;
        this.uporabniskoIme = uporabniskoIme;
        this.geslo = geslo;
        this.email = email;
        this.telefonska = telefonska;
        this.telPotrjena = telPotrjena;
        this.ime = ime;
        this.priimek = priimek;
        avgOcena = 0;
    }

    public Uporabnik(string ime, string priimek)
    {
        this.ime = ime;
        this.priimek = priimek;
    }

    public Uporabnik(string uporabniskoIme, string geslo, string email, string telefonska, string ime, string priimek, string telPotrjena)
    {
        this.TelPotrjena = telPotrjena;
        this.uporabniskoIme = uporabniskoIme;
        this.Geslo = geslo;
        this.email = email;
        this.Telefonska = telefonska;
        this.ime = ime;
        this.priimek = priimek;
        AvgOcena = 0;

    }

    public Uporabnik()
    {
    }

    public void insertUporabnik()
    {
        Int32 id;

        string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/FERIDRIVE.mdf");
        var conn = new SqlConnection();
        conn.ConnectionString =
           " Data Source =.\\SQLEXPRESS;" +
        "AttachDbFilename = " + path + "; " +
        "Integrated Security = True;" +
        "Connect Timeout = 30;" +
        "User Instance = True";
        conn.Open();
        string getID = "SELECT MAX(id_uporabnik) FROM Uporabnik";
        SqlCommand s = new SqlCommand(getID, conn);
        s.CommandText = getID;
        id = (Int32)s.ExecuteScalar();

        string query = "insert into Uporabnik(id_uporabnik,uporabnisko_ime,geslo,email,tel,tel_potrjena,ime,priimek) " +
            "VALUES (@id_uporabnik,@uporabnisko_ime,@geslo,@email,@tel,@tel_potrjena,@ime,@priimek)";

        string queryForId = "SELECT MAX(id_uporabnik) FROM uporabnik";
        Object[,] poljeParametrov = new Object[,]
        {
            { "@id_uporabnik", "0" },
            { "@uporabnisko_ime",uporabniskoIme},
            { "@geslo",Geslo  },
            { "@email",email},
            { "@tel",Telefonska},
            { "@tel_potrjena",TelPotrjena},
            { "@ime",ime },
            { "@priimek",priimek }
         };
        Povprasevanja.insertData(query, poljeParametrov, queryForId);
    }

    public static string testMetod()
    {
        return "Hello world";
    }

    public string testObjectMetod()
    {
        return "Hello world";
    }

    public void UrediProfil(ref string uporabniskoIme, ref string geslo, ref bool potrjena, ref string ime, ref Prevoz seznamPrevozov)
    {
        throw new System.Exception("Not implemented");
    }

    public void PosljiSporocilo(Sporocilo spor)
    {
        Int32 id;

        string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/FERIDRIVE.mdf");
        var conn = new SqlConnection();
        conn.ConnectionString =
           " Data Source =.\\SQLEXPRESS;" +
        "AttachDbFilename = " + path + "; " +
        "Integrated Security = True;" +
        "Connect Timeout = 30;" +
        "User Instance = True";
        conn.Open();
        string getID = "SELECT MAX(id) FROM sporocilo";
        SqlCommand s = new SqlCommand(getID, conn);
        s.CommandText = getID;
        id = (Int32)s.ExecuteScalar()+1;

        string query = "insert into sporocilo(id,id_posiljatelj,id_prejemnik,tekst) " +
            "VALUES (@id,@id_posiljatelj,@id_prejemnik,@tekst)";

        string queryForId = "SELECT MAX(id) FROM sporocilo";
        Object[,] poljeParametrov = new Object[,]
        {
            { "@id", id },
            { "@id_posiljatelj",spor.posiljatelj},
            { "@id_prejemnik",spor.prejemnik  },
            { "@tekst",spor.text}
            
         };
        Povprasevanja.insertData(query, poljeParametrov, queryForId);
        throw new Exception();

    }

    public List<string> PregledProfila()
    {
        List<string> izhodniPodatki = new List<string>
        {
            uporabniskoIme,
            email,
            Telefonska,
            ime,
            priimek
        };
        return izhodniPodatki;
    }

    public void UrejanjePrevozov(ref Prevoz prevoz, ref double cena, ref Kraj krajOd, ref Kraj krajDO, ref DateTime datum)
    {
        throw new System.Exception("Not implemented");
    }

    public void BrisanjePrevoza(ref Prevoz prevoz)
    {
        throw new System.Exception("Not implemented");
    }

    public void DodajanjePrevoza(ref Kraj krajDO, ref Kraj krajOd, ref DateTime datum)
    {
        throw new System.Exception("Not implemented");
    }

    public void ObjavaIskanjaPrevoza(ref object kraj, ref object krajOd, ref object kraj_do, ref object date_datum)
    {
        throw new System.Exception("Not implemented");
    }

    public void OcenjevanjePrevoznika(ref Uporabnik uporabnik, ref Ocena ocena)
    {
        throw new System.Exception("Not implemented");
    }

    public void PregledSeznamaPrevozov()
    {
        throw new System.Exception("Not implemented");
    }

    public void SpremeniOceno(ref int ocenitev, ref String komentar)
    {
        throw new System.Exception("Not implemented");
    }
}