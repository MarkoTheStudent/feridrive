using System;
using WebAppFeriDrive.Models;
public class Prevoz {

	private int cena;
	private Kraj krajOd;
	private Kraj krajDo;
	private DateTime datum;
    private Uporabnik uporabnik;

    private RezultatIskanja[] rezultatIskanjas;

    public Prevoz(int cena, Kraj krajOd, Kraj krajDo, DateTime datum, Uporabnik uporabnik)
    {
        this.Cena = cena;
        this.KrajOd = krajOd;
        this.KrajDo = krajDo;
        this.Datum = datum;     
        this.uporabnik = uporabnik;
        

    }
   
 
    public int Cena { get => cena; set => cena = value; }
    public Kraj KrajOd { get => krajOd; set => krajOd = value; }
    public Kraj KrajDo { get => krajDo; set => krajDo = value; }
    public DateTime Datum { get => datum; set => datum = value; }
    public Uporabnik Uporabnik { get => uporabnik; set => uporabnik = value; }


    public void Samoodstranitev() {
		throw new System.Exception("Not implemented");
	}

	

}
