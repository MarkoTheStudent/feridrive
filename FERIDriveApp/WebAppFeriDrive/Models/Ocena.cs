using System;
using WebAppFeriDrive.Models;

public class Ocena {
	private readonly int id;
	private int ocenitev;
	private String komentar;
    private Uporabnik komentator;
	private Uporabnik uporabnik;

    public Ocena(int ocenitev, string komentar, Uporabnik uporabnik, Uporabnik komentator)
    {
        this.id = id;
        this.Ocenitev = ocenitev;
        this.Komentar = komentar;
        this.Uporabnik = uporabnik;
        this.komentator = komentator;
    }

    public int Id => id;
    public int Ocenitev { get => ocenitev; set => ocenitev = value; }
    public string Komentar { get => komentar; set => komentar = value; }
    public Uporabnik Uporabnik { get => uporabnik; set => uporabnik = value; }
    public Uporabnik Komentator { get => komentator; set => komentator = value; }

    public void insertOcena()
    {
        string query = "INSERT INTO OCENA(id_ocena,ocena,id_uporabnik,komentar,id_prevoz,komentator_id) VALUES (@id_ocena,@ocena,@id_uporabnik,@komentar,'1',@komentator_id)";

        string queryForId = "SELECT MAX(id_ocena) FROM ocena";
        Object[,] poljeParametrov = new Object[,]
        {
            { "@id_ocena", "0" },
            { "@id_uporabnik",uporabnik.Id},
            { "@ocena",ocenitev},
            { "@komentar",komentar },
            { "@komentator_id",komentator.Id},
         
         };
        Povprasevanja.insertData(query, poljeParametrov, queryForId);
    }

}
