using System;
using WebAppFeriDrive.Models;
public class Kraj {
	private int id;
	private string ime;
	private int postnaStevilka;
    

    public Kraj(string ime, int posta)
    {
      
        this.Ime = ime;
        this.PostnaStevilka = posta;
        
    }

    public int insertKraj()
    {
        string query = "INSERT INTO kraj(id_kraj,naziv,posta) VALUES (@id_kraj,@naziv,@posta)";
        string queryCount = "SELECT COUNT(*) FROM KRAJ WHERE naziv=@naziv";
        string queryForId = "SELECT MAX(id_kraj) FROM kraj";
        string[,] poljeParametrov = new string[,]
        {
            { "@id_kraj", "0" },
            { "@naziv", this.ime},
            { "@posta", this.PostnaStevilka.ToString()}
         };
    
        if (!Povprasevanja.valueExists("@naziv", this.ime, queryCount))
        {           
            Povprasevanja.insertData(query, poljeParametrov, queryForId);
            
        }   
            return Povprasevanja.getId("SELECT id_kraj FROM kraj WHERE naziv=@naziv","@naziv",ime);
        
    }

    public string Ime { get => ime; set => ime = value; }
    public int PostnaStevilka { get => postnaStevilka; set => postnaStevilka = value; }
}
