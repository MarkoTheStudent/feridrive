﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;

namespace WebAppFeriDrive.Models
{
    public static class Povprasevanja
    {
        private static string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/FERIDRIVE.mdf");

        private static string connectionString = " Data Source =.\\SQLEXPRESS;" +
            "AttachDbFilename = " + path + "; " +
            "Integrated Security = True;" +
            "Connect Timeout = 30;" +
            "User Instance = True";

        //vrni vse prevoze
        public static List<UporabnikovPrevoz> vrniVsePrevoze(int uporabnikID,DateTime date)
        {
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();
            string query = "";

            if (uporabnikID == -1)
            {
                query = "SELECT mojPrevoz.datum, mojPrevoz.cena,krajO.naziv as krajOdhoda, krajP.naziv as krajPrihoda,id_prevoz FROM prevoz AS mojPrevoz " +
                                "JOIN kraj as krajO " +
                                    "ON krajO.id_kraj = mojPrevoz.krajOdhoda" +
                                " JOIN kraj as krajP " +
                                    "ON krajP.id_kraj = mojPrevoz.krajPrihoda WHERE datum > @datum";
            }
            else
            {
                query = "SELECT mojPrevoz.datum, mojPrevoz.cena,krajO.naziv as krajOdhoda, krajP.naziv as krajPrihoda,id_prevoz FROM prevoz AS mojPrevoz  " +
                                "JOIN kraj as krajO " +
                                    "ON krajO.id_kraj = mojPrevoz.krajOdhoda" +
                                " JOIN kraj as krajP " +
                                    "ON krajP.id_kraj = mojPrevoz.krajPrihoda WHERE id_uporabnik = @id_uporabnik";
            }

            using (var command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@id_uporabnik", uporabnikID);
                command.Parameters.AddWithValue("@datum", date);
                using (var reader = command.ExecuteReader())
                {
                    var list = new List<UporabnikovPrevoz>();
                    while (reader.Read())
                    {
                        Kraj krajOdhoda = new Kraj(reader.GetString(2), 0);
                        Kraj krajPrihoda = new Kraj(reader.GetString(3), 0);
                        if (uporabnikID == -1)
                            list.Add(new UporabnikovPrevoz(reader.GetInt32(4), "", reader.GetInt32(1), krajOdhoda, krajPrihoda, reader.GetDateTime(0), new Uporabnik()));
                        else
                            list.Add(new UporabnikovPrevoz(reader.GetInt32(4), "", reader.GetInt32(1), krajOdhoda, krajPrihoda, reader.GetDateTime(0), getUporabnikByID(uporabnikID)));
                    }
                    return list;
                }
            }
        }

        public static List<Uporabnik> vrniVseUporabnike()
        {
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();

            string query = "SELECT id_uporabnik,uporabnisko_ime,geslo,email,tel,tel_potrjena,ime,priimek FROM uporabnik";

            using (var command = new SqlCommand(query, conn))
            {
                using (var reader = command.ExecuteReader())
                {
                    var list = new List<Uporabnik>();
                    while (reader.Read())
                    {
                        list.Add(new Uporabnik(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetString(7)));
                    }
                    return list;
                }
            }
        }

        public static List<Sporocilo> vrniSporocila(int prejemnik)
        {
            var listSporocila = new List<Sporocilo>();
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();
            string query = "SELECT * from Sporocilo WHERE id_prejemnik=@prejemnik";

            using (var command = new SqlCommand(query, conn))
            {

                command.Parameters.AddWithValue("@prejemnik", prejemnik);

                using (var reader = command.ExecuteReader())
                {

                    while (reader.Read())
                    {

                        listSporocila.Add(new Sporocilo(reader.GetInt32(1), reader.GetInt32(0), getUporabnikByID(reader.GetInt32(1)).uporabniskoIme, reader.GetInt32(2), reader.GetString(3)));
                    }
                    return listSporocila;
                }
            }

            return new List<Sporocilo>();
        }

        public static List<Ocena> ReturnAllGradesByUsername(int id)
        { 
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();

            string query = "SELECT ocena,id_uporabnik,komentar,komentator_id FROM ocena WHERE id_uporabnik = @id ";

            using (var command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@id", id);
                using (var reader = command.ExecuteReader())
                {                
                    var list = new List<Ocena>();
                    while (reader.Read())
                    {
                        
                        list.Add(new Ocena(reader.GetInt32(0),reader.GetString(2),getUporabnikByID(reader.GetInt32(1)),getUporabnikByID(reader.GetInt32(3))));
                    }
                    return list;
                }
            }
        }

    //get id from object in database
    public static int getId(string query, string name, string value)
        {
            // string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/FERIDRIVE.mdf");
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();

            string getID = query;
            SqlCommand sqlCommand = new SqlCommand(getID, conn);
            sqlCommand.Parameters.AddWithValue(name, value);
            Int32 id = (Int32)sqlCommand.ExecuteScalar();
            conn.Close();

            return id;
        }

        //get stuff by their id-s
        public static Uporabnik getUporabnikByID(int id)
        {
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();

            string query = "SELECT uporabnisko_ime,geslo,email,tel,tel_potrjena,ime,priimek,id_uporabnik FROM uporabnik WHERE id_uporabnik = @id";
            Uporabnik u = new Uporabnik();
            using (var command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@id", id);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        u = new Uporabnik(reader.GetInt32(7),reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6));
                    }
                }
                return u;
            }
        }

        public static Uporabnik getUporabnikByUsername(string username)
        {
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();

            string query = "SELECT geslo,email,tel,tel_potrjena,ime,priimek,id_uporabnik FROM uporabnik WHERE uporabnisko_ime = @uporabnisko_ime";
            Uporabnik u = new Uporabnik();
            using (var command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@uporabnisko_ime", username);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        u = new Uporabnik(reader.GetInt32(6),username, reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));
                    }
                }
                return u;
            }
        }

        public static UporabnikovPrevoz getPrevozByID(int id)
        {
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();
            string query = "";

            query = "SELECT mojPrevoz.datum, mojPrevoz.cena,krajO.naziv as krajOdhoda, krajP.naziv as krajPrihoda,mojPrevoz.id_uporabnik FROM prevoz AS mojPrevoz  " +
                            "JOIN kraj as krajO " +
                                "ON krajO.id_kraj = mojPrevoz.krajOdhoda" +
                            " JOIN kraj as krajP " +
                                "ON krajP.id_kraj = mojPrevoz.krajPrihoda WHERE id_prevoz = @id_prevoz ";

            string q = "SELECT id_uporabnik FROM prevoz WHERE id_prevoz = @id";
            string q2 = "SELECT model FROM prevoz WHERE id_prevoz = @id";
            SqlCommand s = new SqlCommand(q, conn);
            s.Parameters.AddWithValue("@id", id);
            int idUporabnika = (Int32)s.ExecuteScalar();

            s = new SqlCommand(q2, conn);
            s.Parameters.AddWithValue("@id", id);
            string model = s.ExecuteScalar().ToString();
            UporabnikovPrevoz up = new UporabnikovPrevoz();
            using (var command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@id_prevoz", id);

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Kraj krajOdhoda = new Kraj(reader.GetString(2), 0);
                        Kraj krajPrihoda = new Kraj(reader.GetString(3), 0);
                        up = (new UporabnikovPrevoz(id, model, reader.GetInt32(1), krajPrihoda, krajOdhoda, reader.GetDateTime(0), getUporabnikByID(idUporabnika)));
                    }
                }

                return up;
            }
        }

        //checks if value exists in table (doesn't insert if exists) - for kraj, uporabnik itd
        public static bool valueExists(string stringForCheck, string valueForCheck, string queryForCheck)
        {
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;

            conn.Open();

            SqlCommand sql;
            string queryForCount = queryForCheck;
            sql = new SqlCommand(queryForCount, conn);
            sql.Parameters.AddWithValue(stringForCheck, valueForCheck);
            int result = (int)sql.ExecuteScalar();

            conn.Close();

            if (result != 0)
                return true;

            return false;
        }

        //general method for inserting data: query = splošno povpraševanje (insert), polje vrednosti = 2d polje za @value, value (priredi vrednosti), queryForId - for autoincrement
        public static void insertData(string query, Object[,] poljeVrednosti, string queryForId)
        {
            Int32 id;
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;

            conn.Open();
            //set id
            string getID = queryForId;
            SqlCommand s = new SqlCommand(getID, conn);
            s.CommandText = getID;
            id = (Int32)s.ExecuteScalar();

            SqlCommand sql;

            string sqlString = query;
            {
                sql = new SqlCommand(sqlString, conn);
                for (int i = 0; i < poljeVrednosti.GetLength(0); i++)
                {
                    if (i == 0)
                        sql.Parameters.AddWithValue(poljeVrednosti[0, 0].ToString(), id + 1);
                    else
                        sql.Parameters.AddWithValue(poljeVrednosti[i, 0].ToString(), poljeVrednosti[i, 1]);
                }
                sql.ExecuteNonQuery();
            }
            conn.Close();
        }

        public static void justARAndomQuery(string query)
        {
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();

            SqlCommand command = new SqlCommand(query, conn);

            command.ExecuteNonQuery();

            conn.Close();
        }

        public static List<UporabnikovPrevoz> vrniIskanePrevoze(string krajO, string krajP, string datum, string ura,DateTime date)
        {
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();


            string query = "SELECT mojPrevoz.datum, mojPrevoz.cena,krajO.naziv as krajOdhoda, krajP.naziv as krajPrihoda,id_prevoz  FROM prevoz AS mojPrevoz " +
                               "JOIN kraj as krajO " +
                                   "ON krajO.id_kraj = mojPrevoz.krajOdhoda" +
                               " JOIN kraj as krajP " +
                                   "ON krajP.id_kraj = mojPrevoz.krajPrihoda WHERE krajO.naziv = @krajO AND krajP.naziv = @krajP AND mojPrevoz.datum=@datumI AND datum>@datum";




            using (var command = new SqlCommand(query, conn))
            {
                string dtStr = datum + " " + ura;
                DateTime dt = DateTime.ParseExact(dtStr, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                command.Parameters.AddWithValue("@krajO", krajO);
                command.Parameters.AddWithValue("@krajP", krajP);
                command.Parameters.AddWithValue("@datumI", dt);
                command.Parameters.AddWithValue("@datum", date);
                var listIskaniPrevozi = new List<UporabnikovPrevoz>();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Kraj krajOdhoda = new Kraj(reader.GetString(2), 0);
                        Kraj krajPrihoda = new Kraj(reader.GetString(3), 0);
                        listIskaniPrevozi.Add(new UporabnikovPrevoz(reader.GetInt32(4), "", reader.GetInt32(1), krajOdhoda, krajPrihoda, reader.GetDateTime(0), new Uporabnik()));
                    }
                    return listIskaniPrevozi;
                }
            }
        }


        public static List<UporabnikovPrevoz> vrniIskanePrevoze2(string krajO, string krajP,DateTime datum)
        {
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();


            string query = "SELECT mojPrevoz.datum, mojPrevoz.cena,krajO.naziv as krajOdhoda, krajP.naziv as krajPrihoda,id_prevoz  FROM prevoz AS mojPrevoz " +
                               "JOIN kraj as krajO " +
                                   "ON krajO.id_kraj = mojPrevoz.krajOdhoda" +
                               " JOIN kraj as krajP " +
                                   "ON krajP.id_kraj = mojPrevoz.krajPrihoda WHERE krajO.naziv = @krajO AND krajP.naziv = @krajP AND datum>@datum";

            using (var command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@krajO", krajO);
                command.Parameters.AddWithValue("@krajP", krajP);
                command.Parameters.AddWithValue("@datum", datum);
                var listIskaniPrevozi = new List<UporabnikovPrevoz>();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Kraj krajOdhoda = new Kraj(reader.GetString(2), 0);
                        Kraj krajPrihoda = new Kraj(reader.GetString(3), 0);
                        listIskaniPrevozi.Add(new UporabnikovPrevoz(reader.GetInt32(4), "", reader.GetInt32(1), krajOdhoda, krajPrihoda, reader.GetDateTime(0), new Uporabnik()));
                    }
                    return listIskaniPrevozi;
                }
            }

        }



        internal static List<UporabnikovPrevoz> vrniIskanePrevoze3(string datum, string ura,DateTime date)
        {
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();


            string query = "SELECT mojPrevoz.datum, mojPrevoz.cena,krajO.naziv as krajOdhoda, krajP.naziv as krajPrihoda,id_prevoz  FROM prevoz AS mojPrevoz " +
                               "JOIN kraj as krajO " +
                                   "ON krajO.id_kraj = mojPrevoz.krajOdhoda" +
                               " JOIN kraj as krajP " +
                                   "ON krajP.id_kraj = mojPrevoz.krajPrihoda WHERE mojPrevoz.datum LIKE '@datumI%' AND datum>@datum";

            using (var command = new SqlCommand(query, conn))
            {
                string dtStr = datum + " " + ura;
                DateTime dt = DateTime.ParseExact(dtStr, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                command.Parameters.AddWithValue("@datumI", dt);
                command.Parameters.AddWithValue("@datum", date);
                var listIskaniPrevozi = new List<UporabnikovPrevoz>();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Kraj krajOdhoda = new Kraj(reader.GetString(2), 0);
                        Kraj krajPrihoda = new Kraj(reader.GetString(3), 0);
                        listIskaniPrevozi.Add(new UporabnikovPrevoz(reader.GetInt32(4), "", reader.GetInt32(1), krajOdhoda, krajPrihoda, reader.GetDateTime(0), new Uporabnik()));
                    }
                    return listIskaniPrevozi;
                }
            }
        }

       

    }
}