using System;
public class JavniPrevoz : Prevoz  {


	private int id;
	private string naziv;

    public JavniPrevoz(int id, string naziv, int cena, Kraj krajOd, Kraj krajDo, DateTime datum) : base(cena,krajOd,krajDo,datum,null)
    {
        this.id = id;
        this.naziv = naziv;
    }
}
