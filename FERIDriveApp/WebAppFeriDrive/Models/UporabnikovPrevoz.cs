using System;
using System.Web.Mvc;
using WebAppFeriDrive.Models;

public class UporabnikovPrevoz : Prevoz  {
	private int id;
	private String naziv;
	private string modelAvta;

    public UporabnikovPrevoz(): base(0,null,null,new DateTime(),null)
    {
    }

    public UporabnikovPrevoz( string modelAvta,int cena, Kraj krajDo, Kraj krajOd,DateTime datum, Uporabnik uporabnik):base(cena,krajDo,krajOd,datum,uporabnik)
    {
        this.ModelAvta = modelAvta;
    }

    public UporabnikovPrevoz(int id,string modelAvta, int cena, Kraj krajDo, Kraj krajOd, DateTime datum, Uporabnik uporabnik) : base(cena, krajDo, krajOd, datum, uporabnik)
    {
        this.id = id;
        this.ModelAvta = modelAvta;
    }

    public ActionResult posiljanjeSporocila(int posiljatelj, int prejemnik, string text)
    {
        Sporocilo s = new Sporocilo(posiljatelj, prejemnik, text);
        Uporabnik u = new Uporabnik();
        u.PosljiSporocilo(s);
        return null;

        
    }

    public int Id { get => id; set => id = value; }
    public string ModelAvta { get => modelAvta; set => modelAvta = value; }

    public void insertPrevoz(int idkrajOdhoda, int idkrajPrihoda, int idUporabnika)
    {
        string query = "INSERT INTO prevoz(id_prevoz,id_uporabnik,krajOdhoda,krajPrihoda,cena,datum,model) VALUES (@id_prevoz, @id_uporabnika,@idKrajOdhoda,@idKrajPrihoda,@cena,@datum,@model)";

        string queryForId = "SELECT MAX(id_prevoz) FROM prevoz";
        Object[,] poljeParametrov = new Object[,]
        {
            { "@id_prevoz", "0" },
            { "@id_uporabnika",idUporabnika},
            { "@idKrajOdhoda", idkrajOdhoda },
            { "@idKrajPrihoda",idkrajPrihoda },
            { "@cena",this.Cena },
            { "@datum",this.Datum},
            { "@model",this.modelAvta},
         };
        Povprasevanja.insertData(query, poljeParametrov, queryForId);

    }
}
