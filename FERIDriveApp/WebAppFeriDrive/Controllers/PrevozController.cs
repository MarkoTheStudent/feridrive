﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppFeriDrive.Models;

namespace WebAppFeriDrive.Controllers
{
    public class PrevozController : Controller
    {
        private static string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/FERIDRIVE.mdf");

        private static string connectionString = " Data Source =.\\SQLEXPRESS;" +
            "AttachDbFilename = " + path + "; " +
            "Integrated Security = True;" +
            "Connect Timeout = 30;" +
            "User Instance = True";
        public ActionResult AddPrevoz() => View();
        private static List<UporabnikovPrevoz> prevozi = new List<UporabnikovPrevoz>();
        [HttpGet]
        public ActionResult DodajanjePrevoza(string model, string cena, string krajPrihoda, string krajOdhoda, string datum, string ura)
        {
            string dtStr = datum + " " + ura;
            DateTime dt = DateTime.ParseExact(dtStr, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
            int c = Int32.Parse(cena);
            Kraj kraj1 = new Kraj(krajPrihoda, 0);
            int idKrajPrihoda = kraj1.insertKraj();
            Kraj kraj2 = new Kraj(krajOdhoda, 0);
            int idKrajOdhoda = kraj2.insertKraj();

            UporabnikovPrevoz novPrevoz = new UporabnikovPrevoz(model, c, kraj1, kraj2, dt, new Uporabnik());
            prevozi.Add(novPrevoz);
            int idUporabnika = Int32.Parse(Session["id"].ToString());
            novPrevoz.insertPrevoz(idKrajOdhoda, idKrajPrihoda, idUporabnika);

            DateTime date = DateTime.Now;
            return View("Prevozi",Povprasevanja.vrniVsePrevoze(-1,date));
        }

        public ActionResult iskanjePrevoza(string krajOdhoda, string krajPrihoda, string datum, string ura)
        {
            if (krajOdhoda != null && krajPrihoda != null && datum != "" && ura != "")
            {
                List<UporabnikovPrevoz> p1 = Povprasevanja.vrniIskanePrevoze(krajOdhoda, krajPrihoda, datum, ura,DateTime.Now);
                return View("Prevozi", p1);

            }
            else if (krajOdhoda != null && krajPrihoda != null && datum == "" && ura == "")
            {
                List<UporabnikovPrevoz> p2 = Povprasevanja.vrniIskanePrevoze2(krajOdhoda, krajPrihoda,DateTime.Now);
                return View("Prevozi", p2);
            }
            else if (krajOdhoda == "" && krajPrihoda == "" && datum != "")
            {
                List<UporabnikovPrevoz> p3 = Povprasevanja.vrniIskanePrevoze3(datum, "00:00",DateTime.Now);
                return View("Prevozi", p3);
            }
            return View("Prevozi");


        }

        public ActionResult Prevozi()
        {

            List<UporabnikovPrevoz> p = Povprasevanja.vrniVsePrevoze(-1,DateTime.Now);

            return View(p);
        }

        public ActionResult Details(int id)
        {

            UporabnikovPrevoz prevoz = Povprasevanja.getPrevozByID(id);
            return View(prevoz);
        }

        public ActionResult UPrevozi()
        {
            int id = Int32.Parse(Session["id"].ToString());
            List<UporabnikovPrevoz> list = Povprasevanja.vrniVsePrevoze(id,DateTime.Now);
            return View("Prevozi", list);
        }
        public ActionResult posiljanjeSporocila(int posiljatelj, int prejemnik, string text, int id)
        {
            /*Sporocilo s = new Sporocilo(posiljatelj, prejemnik, text);
            Uporabnik u = new Uporabnik();
            u.PosljiSporocilo(s);
            */
            /*string connectionString = " Data Source =.\\SQLEXPRESS;" +
          "AttachDbFilename = " + "C:\\Users\\Dejan Gregorc\\Desktop\\gitfaks\\feridrive\\TestMVC\\WebAppFeriDrive\\App_Data\\FERIDRIVE.mdf" + "; " +
          "Integrated Security = True;" +
          "Connect Timeout = 30;" +
          "User Instance = True";*/
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();

            string idqery = "Select MAX(id) from sporocilo";

            SqlCommand c = new SqlCommand(idqery, conn);
            var reader = c.ExecuteReader();
            int idq = 0;
            while (reader.Read())
            {
                idq = reader.GetInt32(0);
                break;
            }
            idq += 1;
            string query = "insert into sporocilo(id, id_posiljatelj, id_prejemnik, tekst) values(" + idq + "," + posiljatelj + "," + prejemnik + ",'" + text + "')";

            conn.Close();


            conn.ConnectionString = connectionString;
            conn.Open();
            using (var command = new SqlCommand(query, conn))
            {

                command.ExecuteNonQuery();

            }
            conn.Close();

            int id1 = Int32.Parse(Session["id"].ToString());
            //List<UporabnikovPrevoz> list = Povprasevanja.vrniVsePrevoze(id1);
            //return View("Prevozi", list);
            UporabnikovPrevoz prevoz = Povprasevanja.getPrevozByID(id);
            return View("Details", prevoz);
        }
    }
    }
