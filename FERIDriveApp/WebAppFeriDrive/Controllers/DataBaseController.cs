﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAppFeriDrive.Controllers
{
    public class DataBaseController : ApiController
    {
        // GET: api/DataBase
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/DataBase/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/DataBase
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/DataBase/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/DataBase/5
        public void Delete(int id)
        {
        }

        

       
    }
}
