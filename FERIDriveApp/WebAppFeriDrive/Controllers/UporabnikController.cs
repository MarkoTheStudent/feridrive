﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using WebAppFeriDrive.Models;
using System.Web.Hosting;
using System.Text;
using System.Net.Mail;

namespace WebAppFeriDrive.Controllers
{
    public class UporabnikController : Controller
    {
        public ActionResult Uporabniki()
        {
            return View(Povprasevanja.vrniVseUporabnike());
        }

        public ActionResult vrniSporocila(int posiljatelj)
        {

            List<Sporocilo> sporocila = Povprasevanja.vrniSporocila(posiljatelj);
            return View("SporocilaV", sporocila);
        }


        private bool validiran = false;

        // GET: Uporabnik
        public ActionResult Registracija() => View();

        public ActionResult Potrjeno(string regEmail)
        {
            ViewBag.regEmail = regEmail;
            return View();
        }

        public JsonResult RegistracijaPotrjeno(string regEmail)
        {
            validiran = true;
            var msg = "Vaš email je varificiran.";
            return Json(msg);
        }

        public ActionResult dodajanjeUporabnika(string username, string userPassword, string email, string phoneNumber, string usrName, string usrSurName)
        {
            buildEmailTemplate(email);
            List<Uporabnik> uporabniki = new List<Uporabnik>();

            Uporabnik u = new Uporabnik(username, userPassword, email, phoneNumber, usrName, usrSurName, "ne");

            u.insertUporabnik();
            uporabniki.Add(u);

            return View("Uporabniki", uporabniki);
        }

        public ActionResult posiljanjeSporocila(int posiljatelj, int prejemnik, string text)
        {
            Sporocilo s = new Sporocilo(posiljatelj, prejemnik, text);
            Uporabnik u = new Uporabnik();
            u.PosljiSporocilo(s);
            
            
            return View("Prevozi");
        }
        private static string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/FERIDRIVE.mdf");
        public ActionResult posiljanjeSporocilaU(int posiljatelj, int prejemnik, string text, int id)
        {
            /*Sporocilo s = new Sporocilo(posiljatelj, prejemnik, text);
            Uporabnik u = new Uporabnik();
            u.PosljiSporocilo(s);
            */
            string connectionString = " Data Source =.\\SQLEXPRESS;" +
           "AttachDbFilename = " + path + "; " +
          "Integrated Security = True;" +
          "Connect Timeout = 30;" +
          "User Instance = True";
            var conn = new SqlConnection();
            conn.ConnectionString = connectionString;
            conn.Open();

            string idqery = "Select MAX(id) from sporocilo";

            SqlCommand c = new SqlCommand(idqery, conn);
            var reader = c.ExecuteReader();
            int idq = 0;
            while (reader.Read())
            {
                idq = reader.GetInt32(0);
                break;
            }
            idq += 1;
            string query = "insert into sporocilo(id, id_posiljatelj, id_prejemnik, tekst) values(" + idq + "," + posiljatelj + "," + prejemnik + ",'" + text + "')";

            conn.Close();


            conn.ConnectionString = connectionString;
            conn.Open();
            using (var command = new SqlCommand(query, conn))
            {

                command.ExecuteNonQuery();

            }
            conn.Close();

            int id1 = Int32.Parse(Session["id"].ToString());
            //List<UporabnikovPrevoz> list = Povprasevanja.vrniVsePrevoze(id1);
            //return View("Prevozi", list);
            Uporabnik u = Povprasevanja.getUporabnikByID(prejemnik);
            List<Ocena> ocene = Povprasevanja.ReturnAllGradesByUsername(u.Id);


            u.Ocene = ocene;
            int sum = 0;
            foreach (Ocena ocena in ocene)
            {
                sum += ocena.Ocenitev;

                if (ocena.Komentator == null)
                    ocena.Komentator = u;

            }
            double avg = (double)sum / (double)ocene.Count;
            double rounded = Math.Round(avg, 2);
            u.AvgOcena = rounded;
            ViewBag.ocene = ocene;
            ViewBag.steviloOcen = ocene.Count();

            return View("Profil", u);
        }


        public void buildEmailTemplate(string regEmail)
        {
            string body = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/EmailTemplate/") + "Text" + ".cshtml");
            var url = "http://localhost:57378/" + "Uporabnik/Potrjeno?regEmail=" + regEmail;
            body = body.Replace("@ViewBag.ConfirmationLink", url);
            //body = body.toString();
            buildEmailTemplate("Vaša registracija je bila uspešna", body, regEmail);
        }

        public void buildEmailTemplate(string subjectText, string bodyText, string regEmail)
        {
            string from, to, bcc, cc, subject, body;
            from = "poskus.forproject@gmail.com";
            to = regEmail.Trim();
            bcc = "";
            cc = "";
            subject = subjectText;
            StringBuilder sb = new StringBuilder();
            sb.Append(bodyText);
            body = sb.ToString();
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(from);
            mail.To.Add(new MailAddress(to));
            if (!string.IsNullOrEmpty(bcc))
            {
                mail.Bcc.Add(new MailAddress(bcc));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                mail.CC.Add(new MailAddress(cc));
            }
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            SendEmail(mail);
        }

        public void SendEmail(MailMessage mail)
        {
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new System.Net.NetworkCredential("poskus.forproject@gmail.com", "administrator!");
            try
            {
                client.Send(mail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Profil(string username)
        {

            Uporabnik u = Povprasevanja.getUporabnikByUsername(username);
            List <Ocena> ocene = Povprasevanja.ReturnAllGradesByUsername(u.Id);
            
           
            u.Ocene = ocene;
            int sum = 0;
            foreach (Ocena ocena in ocene)
            {
                sum += ocena.Ocenitev;

                if (ocena.Komentator == null)
                    ocena.Komentator = u;
               
            }
            double avg = (double)sum / (double)ocene.Count;
            double rounded = Math.Round(avg, 2);
            u.AvgOcena = rounded;
            ViewBag.ocene = ocene;
            ViewBag.steviloOcen = ocene.Count();

            return View(u);
        }

        public ActionResult ProfilUredi(int id)
        {
            Uporabnik u = Povprasevanja.getUporabnikByID(id);
          
            return View(u);
        }

        public ActionResult Vpis() => View();

        public ActionResult Prijava(string username, string userPassword)
        {
            int idUporabnika = Povprasevanja.getId("SELECT id_uporabnik FROM uporabnik WHERE uporabnisko_ime=@username", "@username", username);

            Uporabnik uporabnik = Povprasevanja.getUporabnikByID(idUporabnika);

            if (uporabnik.Geslo == userPassword)
            {
                Session["name"] = username;
                Session["id"] = idUporabnika;

                return RedirectToAction("Prevozi", "Prevoz");
            }
            else
            {
                ViewBag.message = "napaka";
                return View("Vpis");
            }
        }

        public ActionResult Logout()
        {
            Session.Abandon();

            return View("Vpis");
        }

        public ActionResult UporabnikoviPrevozi()
        {
            return RedirectToAction("UPrevozi", "Prevoz");
        }

        public ActionResult dodajOceno(string gradOpt,string komentar,string uporabniskoIme)
        {
           
            if (komentar == null)
                komentar = "";
            Uporabnik u = Povprasevanja.getUporabnikByUsername(uporabniskoIme);
            Uporabnik prijavljen = Povprasevanja.getUporabnikByID(Int32.Parse(Session["id"].ToString()));
            Ocena ocena = new Ocena(Int32.Parse(gradOpt),komentar,u,prijavljen);
            ocena.insertOcena();
            return RedirectToAction("Profil", "Uporabnik",new { username = u.uporabniskoIme });
        }

        
    }
}