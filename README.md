﻿# FERIdrive
## Vizija
## 1. Uvod
Obstajajo razne aplikacije tako za javne prevoze kot tudi privatne, ni pa možnosti s katero bi uporabniki imeli to dvoje povezano, prav tako pa tudi spletne verzije aplikacij niso uporabnikom najbolj prijazne. Zato smo se odločili, da bomo naredili spletno aplikacijo, ki bo združevala lokalni javni promet in zasebni prevoz med mesti. 
## 2. Opredelitev
### 2.1 Problem

Problem| Udeleženci| Vpliv|Uspešna rešitev
-------|-----------|------|---------------
Problem	Težko iskanje optimalnega prevoza v mestu in med mesti na do sedaj razvitih aplikacijah | Udeleženci	Vsi uporabniki javnih prevoznih sredstev in ponudniki prevozov | Vpliv	Zamujanje na destinacijo, zaradi slabe koherentnosti med posamezni prevozi, dolgotrajno brskanje med različnimi ponudniki prevozv v različnih mestih  | Uspešna rešitev	Enostavnejše in hitrejše iskanje željenega prevoza, povezovanje različnih spletnih strain, ki že ponujajo prevoze, lažje iskanje prevoza, ki ustraza posamezniku
### 2.2 Izdelek

Je namenjen | Kdaj | Ime produkta	| Ki | Kot na primer |	Naš produkt |	
------------|------|----------------|----|---------------|--------------|
Uporabnikom javnih prevozov in oglaševalcem oz. organizatorjem prevozov | Kdaj	Ob iskanju določenega prevoza | FeriDrive | Je aplikacija za iskanje prevoza  in ga poenostavi | Prevozi.org, slo-zeleznice,izletnik.si, marprom.si, arriva.si | Je uporabniku bolj prijazen in mu omogoča lažje iskanje prevoza, ki mu resnično ustreza

## 3. Opisi vlog
### 3.1 Povzetki vlog uporabnikov
Vloga|	Opis vloge|	Možnosti
-----|------------|--------
Neregistriran uporabnik|	Vsak uporabnik, ki želi uporabljati aplikacijo|	Išče prevoze in v primeru osebnih prevozov kontaktira prevoznika.
Registriran uporabnik|	Uporabnik, ki se ob vstopu v aplikacijo prijavi tako da ima ob naslednjem vstopu bolj personalizirano spletno stran|Išče prevoze, kontaktira ponudnika, si shrani prevoz, oceni in komentira prevoznika
Osebni prevoznik|	Uporabnik, ki ponuja storitev drugim uporabnikom|Objavi termin in linijo prevoza, le-ta prevoz tudi izvede
Javni prevozi|	Nudijo prevoze v različnih krajih, prav tako tudi medkrajevne|Njhiovi termini prevozijo se združijo in povežejo z ponudniki ostalih “osebnih” prevozov


### 3.2 Uporabniško okolje
Uporabnik vsako dejanje opravi sam. Čas iskanja prevoza ali objava prevoza mora biti čim manjši. Omejitev z dostopom do interneta. Aplikacija deluje na spletnem brskalniku, v prihodnosti možnost mobilne verzije. Aplikacija je povezana z google zemljevidi, za prikaz poti prevoza.

## 4. Pregled produkta
### 4.1 Perspektiva produkta
V primerjavi z drugimi produkti je naš bolj prijazen uporabniku in ponuja širši nabor storitev, medtem ko so drugi produkti fokusirani na le en tip prevoza(vlak, avtobus, osebni…)

### 4.2 Predvidevanja in odvisnosti
Aplikacija jje odvisna od brskalnika in povezave do interneta, v primeru nadaljevanja na mobilno platformo tudi od verzije mobilne platforme(android, IOS…)

### 4.3 Potrebe in funkcionalnosti
Potreba|	Prioriteta| 	Funkcionalnost|	Planirana izdaja
-------|--------------|-------------------|-----------------
Iskanje prevoza|	Visoka|	Seznam in filtriranje možnih prevozov|	Ob koncu projekta
Dodajanje prevoza| 	Visoka|	Obrazec za dodajanje termina prevoza|	Ob koncu projekta

### 4.4 Alternative in konkurenca
*   Prevozi.org
*	slo-železnice
*	izletnik.si
*	marprom.si
*	arriva.si
*	Ljubljanski potniški promet

## 5. Ostale zahteve produkta
Zahteve za delovanje:
*   Spletni brskalnik
